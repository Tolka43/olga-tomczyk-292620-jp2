class Library {

    constructor(books) {
        this.books = books
    }

    addBook(title, author) {
        this.books.push({'title': title, 'author': author})
    }

    findBookByAuthor(author) {
        return this.books.filter(obj => obj.author === author)
    }
    sortBooksByTitle() {
        this.books.sort(obj => obj.title)
    }

    removeBook(title) {
        let bookToRemove = this.books.find(a => a.title === title)
        let id = this.books.indexOf(bookToRemove)
        this.books[id] = {}
    }
}

const myLibrary = new Library([]);

myLibrary.addBook('Harry Potter', 'J.K. Rowling');
myLibrary.addBook('The Great Gatsby', 'F. Scott Fitzgerald')
myLibrary.addBook('Lord of the Rings', 'J.R.R. Tolkien');

myLibrary.findBookByAuthor('J.K. Rowling')

myLibrary.sortBooksByTitle()

myLibrary.removeBook('Lord of the Rings')
