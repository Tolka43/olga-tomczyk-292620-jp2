const list = require('./shopping-list').shoppingList;


console.log(
  list.reduce((acc, obj) => {
    const { quantity, price, productName, productType,} = obj;
    if (!acc[productType]) {
      acc[productType] = {};
    }

    // Dodaj produkt do odpowiedniego typu
    acc[productType] = { totalValue: price*quantity, quantity, };

    return acc;
  }, {})
);